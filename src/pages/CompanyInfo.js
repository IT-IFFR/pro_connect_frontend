import React from 'react'
import { useLocation } from 'react-router-dom'

import Profile from '../components/Profile/Profile'
import logo from '../assets/icons/logo.svg'

const CompanyInfo = () => {

    // const {id} = useParams()

    const {
        company, 
        companyProfile, 
        country,
        email,
        website,
        territories,
        mainWorkingSector,
        otherWorkingSector,
        focus,
        objectives,
        attendingGuests
    } = useLocation().state.company
    console.log(company)
    return (
        <div className='company_detail_info'>
            <div className='info_upper'>
                <h2>{company}</h2>
            </div>
            <div className='info_mid'>
                <div className='info_mid_left'>
                    <p><strong>Company profile</strong></p>
                    {companyProfile}
                    {/* {
                        focus !== null || focus !== '' ?
                        (`<p><strong>Focus</strong></p> ${focus}`) : null
                    }
                    
                    {
                        objectives !== null || objectives !== '' ?
                        (`<p><strong>Objectives</strong></p> ${objectives}`) : null
                    } */}
                </div>

                <div className='info_mid_right'>
                    {/* <p><strong>Country</strong>: {country}</p> */}
                    <ul>
                        <li><strong>Country</strong>: {country}</li>
                        <li><strong>Email</strong>: {email}</li>
                        <li><strong>Website</strong>: {website}</li>
                        <li><strong>Territories</strong>: {territories}</li>
                        <li><strong>Main sector</strong>: {mainWorkingSector}</li>
                        <li><strong>Other sectors</strong>: {otherWorkingSector}</li>
                    </ul>
                </div>
            </div>
            <div className='info_guests'>
                {
                    attendingGuests.map((guest, index) => (
                        <Profile key={index}
                            url={null}
                            name={guest.name}
                            role={guest.role}
                            badges={guest.badges}
                        />

                    ))
                }
            </div>
        </div>
    )
}

export default CompanyInfo
