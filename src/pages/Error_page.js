import React from 'react'

const Error_page = () => {


    return (
        <div className=" content notfound">
            <h1>Sorry, we can't find what you are looking for</h1>
        </div>
    )
}

export default Error_page
