import React, { useContext, useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'

import {DataContext} from '../context/ContextProvider'
import ErrorPage from '../pages/Error_page'

import ChatUser from '../components/Chat/ChatUser'
import ChatWindow from '../components/Chat/ChatWindow'

const Single_event = () => {

    const {io, name} = useContext(DataContext)

    const {event} = useParams();

    const [ participantList, setParticipantList ] = useState([])  
    const [ chatLog, setChatLog ] = useState([])
    
    const [ msg, setMsg ] = useState('')
    const [ emptyMsg, setEmptyMsg ] = useState(true)

    const events = ['Pro Hub', 'CineMart', 'Reality Check', 'Rotterdam Lab']
   
    useEffect( () => {
        io.emit('joinRoom',{name, event, chatLog})
        io.on(event, msg => setParticipantList(msg))
        io.on('room message',msg => {
            setChatLog(chatLog => [...chatLog, msg])
        })

        // cleanup function
        return () => {
            io.emit('leave room')
        }
    }, [])
    

    const sendChat = (e) => {
        e.preventDefault()
        io.emit('room message', msg)
        setMsg('')
        setEmptyMsg(true)
    }

    const onMsgChange = (e) => {
        setMsg(e.target.value)
        setEmptyMsg(false)
    }

    if(events.includes(event)){
        return (
            <div className='content event'>
                <div className='event_header'>
                    <h1>{event}</h1>
                </div>
                <div className="event_chat">
                    <ChatUser activeUsers={participantList} roomName={event}/>
                    <ChatWindow 
                        chatLog={chatLog} 
                        msg={msg} 
                        setMsg={setMsg} 
                        emptyMsg={emptyMsg} 
                        onMsgChange={onMsgChange}
                        sendChat={sendChat}
                    />
                </div>
            </div>
        )
    }else{
        return (
                <ErrorPage />
        )
    }

}

export default Single_event