import React, { useState, useEffect } from 'react'
import socketIOClient from 'socket.io-client';

export const DataContext = React.createContext();

// socket.io endpoint to the entire back-end server
// const globalEnd = 'http://localhost:4000'
const globalEnd = 'https://desolate-falls-00779.herokuapp.com'

// connect to global endpoint
const io = socketIOClient(globalEnd, { transports: ['websocket']});

// const name = window.prompt("what is your chat name?")
const name = 'Sonny'

const DataProvider = (props) => {

    // const APIurl = 'http://www.json-generator.com/api/json/get/bTXxfIDzVK?indent=2'
    // const APIurl = 'http://localhost:4000/proconn/api/companies'
    const APIurl = 'https://desolate-falls-00779.herokuapp.com/proconn/api/companies'

    let [dataFetched, setDataFetched] = useState(false)
    let [companyData, setCompanyData] = useState(null)

    const[favourites, setFavourites] = useState([])

    // Change this into fetching data from backend api url
    // const companies = Data.data.map(companie => companie)

    // function to be used to fetch the data
    const FetchingData = async(url) => {
        try{
            const response = await fetch(url,{mode: "cors"})
            const data = await response.json()
            if(!response.ok){
                throw Error(response.statusText)
            }
            return data
        }catch(err){
            console.log(err)
        }
    }

    useEffect( () => {
        FetchingData(APIurl)
        .then(data => {
            if(!data){
                return setCompanyData([])
            }
            setDataFetched(true)
            return setCompanyData(data.data)
        })
    },[])

    return(
        <DataContext.Provider value={{
            dataFetched,
            companyData,
            io,
            name
        }}>
            {props.children}
        </DataContext.Provider>
    )

}

const DataConsumer = DataContext.Consumer;

export {DataProvider, DataConsumer};