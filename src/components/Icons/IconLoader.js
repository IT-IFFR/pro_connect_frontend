import React, { useState } from 'react'

const IconLoader = ({title, icon_className}) => {
    
    title = title.toLowerCase()

    let [ favourite, setFavourite ] = useState(false)

    function changefavourite(){
        setFavourite(!favourite)
    }


   switch(title){
       case "calendar":
           return (
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className={`${icon_className} feather feather-calendar`}><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
           )
        case "close":
            return(
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className={`${icon_className} feather feather-x`}><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
            )
        case "home":
            return(
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className={`${icon_className} feather feather-home`}><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
            )
        case "star":
            return(
                <svg onClick={changefavourite} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className={`${icon_className} ${favourite ? 'favourite' : ''} feather feather-star`}><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
            )
        case "youtube":
            return(
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className={`${icon_className} feather feather-youtube`}><path d="M22.54 6.42a2.78 2.78 0 0 0-1.94-2C18.88 4 12 4 12 4s-6.88 0-8.6.46a2.78 2.78 0 0 0-1.94 2A29 29 0 0 0 1 11.75a29 29 0 0 0 .46 5.33A2.78 2.78 0 0 0 3.4 19c1.72.46 8.6.46 8.6.46s6.88 0 8.6-.46a2.78 2.78 0 0 0 1.94-2 29 29 0 0 0 .46-5.25 29 29 0 0 0-.46-5.33z"></path><polygon points="9.75 15.02 15.5 11.75 9.75 8.48 9.75 15.02"></polygon></svg>
            )
        case "logo":
            return(
                <svg height="25" viewBox="0 0 500 500" width="25" xmlns="http://www.w3.org/2000/svg" className={icon_className}>
                    <path d="m53.963 93.598c-2.438-4.878-4.268-10.671-4.268-17.378 0-.305 0-.61 0-.915.305-13.11 4.573-25.915 
                        17.073-36.585 11.586-10.061 23.171-13.415 34.452-13.415h.305c8.536 0 16.158 2.439 23.476 7.622l1.22.915-1.22.61c-26.83 
                        15.243-50.305 35.67-69.513 59.146l-.914 1.219zm390.244 0c-19.207-23.781-42.683-43.903-69.207-59.452l-1.22-.61 1.22-.915c7.012-4.878 
                        14.938-7.317 23.476-7.622h.305c11.28 0 22.561 3.354 34.451 13.415 12.195 10.976 16.768 23.476 17.073 36.586v1.22c0 6.707-1.829 
                        12.5-4.269 17.378l-.609 1.219zm-258.536 92.073v.609l-.61.305c-.914.61-2.134 1.524-3.354 2.134-3.658 2.134-7.927 3.658-12.805 
                        3.658-3.354 0-7.013-.609-11.28-3.049-3.659-1.829-5.793-4.573-7.622-7.317-1.524-3.049-2.439-6.708-2.439-10.671 0-4.573 
                        1.22-10.061 3.659-15.244l.305-.61s7.316 2.744 10.365 3.963c8.537 4.269 14.33 8.537 17.378 12.805 2.439 3.049 4.573 7.317 
                        5.793 12.805.304.002.304.307.61.612m128.353.914v-.61c0-.305.306-.915.306-1.22 1.219-5.488 3.658-9.756 5.792-12.805 
                        3.354-4.268 8.842-8.537 17.378-12.5 2.744-1.524 5.793-2.744 9.146-3.659l.915-.305.305.61c2.439 5.183 3.658 10.366 
                        3.658 14.939 0 4.268-.914 7.927-2.438 10.976s-3.964 5.488-7.622 7.317c-4.269 2.439-7.927 3.049-11.28 3.049-5.183 
                        0-9.756-1.829-13.415-4.268-.914-.61-1.524-.915-2.134-1.22zm-105.488 123.171-.914-1.524h84.451l-.915 1.524c-11.585 
                        17.379-25.609 36.281-40.549 54.573l-.609.915-.61-.915c-15.243-18.292-28.963-37.5-40.854-54.573m-58.536 141.159c-21.342-10.976-41.769-25-58.537-41.769-1.829-1.83-3.658-3.658-5.487-5.488l-.915-.914.915-.609c14.938-9.146 54.573-33.842 60.671-37.5 
                        1.829-1.221 3.354-3.354 3.354-5.488 0-.915-.305-1.83-.915-3.049l-7.012-11.281c-1.524-2.133-3.354-3.049-5.488-3.049-1.22 0-2.744.306-3.658.916l-64.329 39.938-.61-.61c-3.049-4.268-6.098-8.841-9.146-13.414l-.609-.914.915-.61c10.061-6.098 
                        57.621-35.671 64.329-39.634 1.829-1.22 3.354-3.354 3.354-5.488 0-.914-.305-1.829-.915-3.049l-7.012-11.28c-1.524-2.134-3.354-3.049-5.488-3.049-1.219 0-2.743.305-3.658.915l-63.72 
                        39.328-.305-.914c-2.134-4.878-4.269-10.061-6.402-14.939l-.305-.609.609-.305c14.634-9.146 54.573-33.842 60.671-37.5 
                        1.829-1.22 3.354-3.354 3.354-5.488 0-.914-.305-1.829-.915-3.049l-7.012-11.28c-1.524-2.134-3.354-3.048-5.488-3.048-1.219 0-2.743.305-3.658.914l-54.573 
                        34.756-.305-1.219c-3.964-16.464-6.098-33.842-6.098-51.525 0-61.89 25-117.987 65.549-158.536 27.744-27.744 62.5-48.171 101.22-58.232l1.22-.305v70.122c0 3.354 2.134 6.402 
                        5.792 6.707h13.11c4.268 0 6.402-3.658 6.402-6.707v-75.61h.914c5.184-.61 10.671-1.22 16.159-1.524h.914v77.134c0 3.354 2.135 6.402 5.793 6.707h13.109c4.269 0 6.403-3.658 
                        6.403-6.707v-77.136h.914c5.488.305 10.671.915 16.159 1.524h.914v75.61c0 3.354 2.134 6.402 5.793 6.707h13.109c4.269 0 6.402-3.658 6.402-6.707v-70.427l1.22.305c39.024 
                        10.366 73.476 30.792 101.22 58.232 40.549 40.548 65.549 96.646 65.549 158.536 0 17.684-2.135 35.061-6.098 51.524l-.306 1.22-55.487-34.451c-.915-.609-2.134-.914-3.658-.914-2.135 0-3.964.914-5.488 
                        3.048l-7.012 11.28c-.61.915-.915 2.135-.915 3.049 0 2.135 1.22 4.269 3.354 5.488 6.098 3.658 46.037 28.354 60.671 37.5l.609.305-.305.609c-1.829 
                        5.184-3.963 10.062-6.402 14.939l-.305.914-63.72-39.328c-.915-.61-2.134-.915-3.658-.915-2.135 0-3.964.915-5.488 3.049l-7.012 11.28c-.61.914-.915 
                        2.135-.915 3.049 0 2.135 1.524 4.269 3.354 5.488 6.708 3.963 54.269 33.536 64.329 39.634l.915.61-.609.914c-2.744 4.573-5.793 9.146-9.146 13.414l-.61.61-64.329-39.938c-.914-.61-2.134-.916-3.658-.916-2.134 0-3.964.916-5.488 
                        3.049l-7.012 11.281c-.609.914-.915 2.134-.915 3.049 0 2.134 1.524 4.268 3.354 5.488 6.098 3.658 45.732 28.354 60.671 37.5l.915.609-.61.914c-1.829 1.83-3.658 3.658-5.487 5.488-16.769 
                        16.769-36.28 31.098-57.927 41.769l-.305.305-.306-.305c-6.402-2.744-28.048-13.11-60.365-42.684-7.622-7.012-14.938-14.634-22.256-22.866l-.61-.609.61-.609c23.476-28.354 43.902-58.842 
                        59.146-81.707 1.524-2.439 2.439-5.184 2.439-8.232 0-1.829-.305-3.354-.609-5.183-6.098-21.036-13.11-49.085-14.939-73.171v-1.524l1.22.61c4.573 1.524 10.061 2.744 15.854 2.744 7.317 
                        0 15.549-1.83 23.476-6.098 7.622-4.269 13.72-10.366 17.683-17.378 3.964-7.012 5.793-14.939 5.793-22.866 0-5.792-.915-11.585-2.744-17.073l-.305-1.524 1.524.305c7.013.915 13.72 
                        3.354 20.731 7.927l.915.61c1.22.61 2.134 1.219 3.658 2.134s3.049 1.219 4.573 1.219c1.829 0 3.049-.61 3.964-1.829 2.134-3.049 4.878-7.317 7.622-11.281.609-.915.914-1.829.914-3.049 
                        0-2.134-1.22-4.268-2.744-5.183-1.523-1.22-3.658-2.744-5.487-3.963-14.024-8.842-28.658-12.5-42.683-12.5h-.306c-14.938 0-28.658 3.963-40.243 9.756-11.586 5.793-20.732 13.11-27.135 
                        21.037-4.878 6.402-7.926 13.72-9.756 21.646-1.829 7.927-2.438 16.463-2.438 25.305 0 25 5.487 53.354 12.194 77.134l.306 1.221h-98.171l.305-1.221c6.707-24.085 11.891-52.438 
                        12.195-77.134 0-8.841-.609-17.378-2.439-25.305-1.829-7.927-4.573-15.244-9.756-21.646-6.402-7.927-15.549-15.244-27.134-21.037-11.586-5.792-25.305-9.756-40.244-9.756h-.305c-13.72 
                        0-28.659 3.658-42.684 12.5-1.829 1.219-3.963 2.744-5.487 3.963s-2.744 3.354-2.744 5.183c0 .915.305 2.134.915 3.049 2.744 3.963 5.487 8.232 7.622 11.281.914 1.219 2.134 1.829 
                        3.963 1.829 1.524 0 3.354-.61 4.573-1.219 1.524-.915 2.439-1.524 3.658-2.134l.915-.61c6.707-4.269 13.415-6.708 20.122-7.622l1.524-.305-.308 1.525c-1.829 5.488-2.744 11.28-2.744 
                        17.073 0 7.927 1.829 15.854 5.793 22.866 3.963 7.012 9.756 13.11 17.683 17.378 7.927 4.269 16.158 6.098 23.476 6.098 6.098 0 11.891-1.22 16.464-3.049l1.219-.61v1.524c-1.524 
                        24.085-8.536 52.135-14.938 73.171-.61 1.829-.915 3.659-.915 5.487 0 2.744.61 5.488 2.135 7.928 15.243 23.475 35.67 53.658 59.146 82.012l.61.609-.61.61c-7.316 8.231-14.938 
                        15.854-22.256 22.866-32.014 29.269-53.66 39.635-60.063 42.379m100 23.78c-23.78 0-46.646-3.659-67.988-10.366l-1.829-.609 1.524-.915c11.891-7.622 27.439-18.902 45.731-35.976 
                        7.317-6.707 14.635-14.329 21.646-21.951l.61-.609.609.609c7.012 7.622 14.329 15.244 21.646 21.951 18.292 17.073 33.231 28.049 45.121 35.671l2.135 1.524-1.829.609c-20.729 
                        6.403-43.595 10.062-67.376 10.062m213.414-353.658-.305-.61.305-.61c7.317-12.195 12.195-27.744 12.195-44.207 
                        0-.305 0-.305 0-.61 0-18.902-7.622-39.634-25.609-55.488-15.549-13.72-33.537-19.512-50-19.512h-.61c-.305 0-.914 0-1.22 0-14.024.305-26.829 3.963-37.5 9.451-5.792 
                        2.744-10.976 6.098-15.243 9.756l-.61.305-.609-.305c-29.269-11.89-60.976-18.597-94.512-18.597-33.537 0-65.244 6.707-94.513 
                        18.598l-.609.305-.306-.305c-4.572-3.354-9.451-6.708-15.243-9.756-10.671-5.488-23.476-9.146-37.5-9.451-.305 0-.915 0-1.22 0-16.464-.001-34.451 5.791-50.305 19.511-17.988 
                        15.549-25.61 36.586-25.61 55.488v.61c0 16.463 4.878 32.012 12.195 44.207l.305.61-.305.61c-23.171 37.5-36.28 82.012-36.28 129.268 0 138.109 111.891 249.695 
                        249.695 249.695s249.694-111.891 249.694-249.695c.001-47.256-13.413-91.768-36.28-129.268">
                    </path>
                </svg>
            )
            case 'list':
                return(
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className={`${icon_className} feather feather-list`}><line x1="8" y1="6" x2="21" y2="6"></line><line x1="8" y1="12" x2="21" y2="12"></line><line x1="8" y1="18" x2="21" y2="18"></line><line x1="3" y1="6" x2="3.01" y2="6"></line><line x1="3" y1="12" x2="3.01" y2="12"></line><line x1="3" y1="18" x2="3.01" y2="18"></line></svg>
                )

            case 'chat':
                return(
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className={`${icon_className} feather feather-message-square`}><path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg>
                )
            case 'profile':
                return(
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className={`${icon_className} feather feather-user`}><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                )
            case 'down arrow':
                return(
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className={`${icon_className} feather feather-chevron-down`}><polyline points="6 9 12 15 18 9"></polyline></svg>
                )
            case 'send':
                    return(
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className={`${icon_className} feather feather-send`}><line x1="22" y1="2" x2="11" y2="13"></line><polygon points="22 2 15 22 11 13 2 9 22 2"></polygon></svg>
                    )
        default:
            return (
                <p>no image found</p>
            )
   }
}

export default IconLoader