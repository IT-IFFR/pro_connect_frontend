import React, { useState } from 'react'

const SearchInput = ({onInput, placeholder}) => {

    const [ query, setQuery ] = useState('')

    const handleSearchInput = (e) => {
        setQuery(e.target.value)
        onInput(e.target.value)
    }

    return (
        <div className="search_input">
            <input type='text' value={query} onChange={handleSearchInput} placeholder={placeholder}/>
        </div>
    )
}

export default SearchInput
