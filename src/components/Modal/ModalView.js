import React, { useState} from 'react'
import Modal from 'react-modal';

const ModalView = ({closeModal, handleModalInput}) => {

    return (
        <div className='modal_view'>
            <div className="modal_view_input">
                <span onClick={closeModal} className="modal_view_close">X</span>
                <form>
                    <label>
                        description
                    </label>
                </form>
            </div>
      </div>
    )
}

export default ModalView
