import React from 'react'
import IconLoader from '../Icons/IconLoader'

import {NavLink} from 'react-router-dom'

export const Navigation = () => {
    return (
        <div className='navigation'>
            <NavLink exact to='/profile' className='navigation_home' activeClassName='active'>
                <IconLoader title='profile' icon_class='navigation_user_icon'/>
                <span>Profile</span>
            </NavLink>
            <NavLink exact to='/' className='navigation_home' activeClassName='active'>
                <IconLoader title='home' icon_class='navigation_home_icon'/>
                <span>Home</span>
            </NavLink>
            <NavLink to='/participants' className='navigation_event' activeClassName='active'>    
                <IconLoader title='youtube' icon_class='navigation_event_icon'/>
                <span>Participants</span>
            </NavLink>
            <NavLink to='/companies' className='navigation_list' activeClassName='active'>    
                <IconLoader title='list' icon_class='navigation_list_icon'/>
                <span>Guests</span>
            </NavLink>
            <NavLink to='/meetings' className='navigation_calendar' activeClassName='active'>    
                <IconLoader title='calendar' icon_class='navigation_calendar_icon'/>
                <span>Meetings</span>
            </NavLink>
        </div>
    )
}
