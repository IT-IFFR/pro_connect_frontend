import React from 'react'
import IconLoader from '../Icons/IconLoader'

export const Header = () => {
    return (
        <div className="header">
            <IconLoader title="logo" icon_class="header_logo" />
            <h3>Pro Connect</h3>
        </div>
    )
}
