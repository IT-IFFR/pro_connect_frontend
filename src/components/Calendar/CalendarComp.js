import React, {useState} from 'react'
// import Calendar from 'react-calendar'
// import 'react-calendar/dist/Calendar.css';

// import Events from './Events'
import FullCal from './FullCal'

const CalendarComp = () => {

    // const [ date, setDate ] = useState(new Date())

    // const onChange = date => {
    //     setDate(date)
    //     console.log(date)
    // }

    return (
        <div className="calendar">
            <div className='calendar_title'>
                <h2>Tiger Calendar</h2>
            </div>
            <FullCal />
            {/* <Calendar
            className='schedule_calendar'
            onChange={onChange}
            value={date}
            />
            <Events date={date}/> */}
        </div>

    )
}

export default CalendarComp
