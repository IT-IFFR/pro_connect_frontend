import React, { useState } from 'react'
import Button from '../Button/Button'

const Events = ({date}) => {

    const gapi = window.gapi
    
    const client_id = "438438894444-e2lg68t73g8gc8ocks0tlsnfc4gjfq1g.apps.googleusercontent.com"
    const client_api = 'AIzaSyBJ_fDYcKDWH2tgFJ2SbTH1j4S2nJfRa88'
    const scopes = 'https://www.googleapis.com/auth/calendar'
    const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

    const [ eventValue, setEventValue ] = useState({
        summary: '',
        location: '',
        description: '',
        start: {
            dateTime: '',
            timeZone: "Europe/Amsterdam"
        },
        end: {
            dateTime: '',
            timeZone: "Europe/Amsterdam"
        },
        attendees: [
            {
                email: 'Sonnylo@hotmail.com'
            },
            {
                email: 's.lo@iffr.com'
            }
        ],
        reminders: {
            useDefault: false,
            overrides: [
              {method: 'email', minutes: 24 * 60},
              {method: 'popup', minutes: 10}
            ]
          }
    })

    // const handleAuth = () => {
    //     gapi.load('client:auth2', () => {
    //       console.log('loaded')
  
    //       gapi.client.init({
    //         apiKey: client_api,
    //         clientId: client_id,
    //         discoveryDocs: DISCOVERY_DOCS,
    //         scope: scopes
    //       })
  
    //       gapi.client.load('calendar', 'V3', () => console.log('calendar loaded'))
  
    //       gapi.auth2.getAuthInstance().signIn()
    //       .then( () => {
  
    //         const request = gapi.client.calendar.events.insert({
    //           'calendarId': 'primary',
    //           'resource': event
    //         });
  
    //         request.execute(event => {
    //           window.open(event.htmlLink)
              
    //         })
    //       })
  
    //     })
    //   }


    const inputHandler = (e) => {
        setEventValue({[e.target.name]: e.target.value})
    }

    const changeDate = (e) => {
        setEventValue({[e.target.name]: {dateTime: e.target.value,
            timeZone: "Europe/Amsterdam"}})
    }

    // const FormRender = () => {
    //     return(
    //        <form>
    //            <label>
    //                Description:
    //                 <input type='text' name="description" value={eventValue.description} onChange={inputHandler}/>
    //            </label>
    //        </form>
    //     )
    // }

    return (
        <div className='calendar_meetings'>
            <div className='schedule_title'>
                <p>{`Meetings on ${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`}</p>
            </div>
            <div className='form_input'>
                    <form>
                        <label>
                            Summary:
                                <input type='text' name="summary" value={eventValue.summary} onChange={inputHandler}/>
                        </label>
                        <label>
                            Location:
                                <input type='text' name="location" value={eventValue.location} onChange={inputHandler}/>
                        </label>
                        <label>
                            Description:
                                <input type='text' name="description" value={eventValue.description} onChange={inputHandler}/>
                        </label>
                        <label>
                            Start time:
                                <input type='datetime-local' name="start" onChange={changeDate}/>
                        </label>
                        <label>
                            End time:
                                <input type='datetime-local' name="end" onChange={changeDate}/>
                        </label>
                    </form>
                </div>
        </div>
    )
}

export default Events
