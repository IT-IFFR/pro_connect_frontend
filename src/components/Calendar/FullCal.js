import React, { useState, useEffect } from 'react'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from "@fullcalendar/interaction";
import timeGridPlugin from '@fullcalendar/timegrid';
import googleCalendarPlugin from '@fullcalendar/google-calendar';

import ApiCalendar from 'react-google-calendar-api'

import ModalView from '../Modal/ModalView'

const FullCal = () => {
    console.log(ApiCalendar)

    const [ calEvent, setCalEvent ] = useState([])
    const [ gapi, setGapi ] = useState()

    useEffect( () => {
        if(!ApiCalendar.sign){
            ApiCalendar.handleAuthClick();
        }

        if(ApiCalendar.sign){
            // console.log(ApiCalendar.listUpcomingEvents(10, 'primary').then(({result}) => result.items))
            ApiCalendar.listUpcomingEvents(10, 'primary').then(({result}) => setCalEvent([result.items]))
            console.log(ApiCalendar)
        }
        

        // return function cleanup(){
        //     ApiCalendar.handleSignoutClick()
        // }
    },[])

    const [modalIsOpen,setIsOpen] = useState(false)

    const closeModal = (e) => {
        setIsOpen(false);
        document.body.classList.remove("no_scroll")
    }

    const handleModalInput = (e) => {
        console.log(e)
    }

    const handleDateClick = (e) => {
        setIsOpen(true);
        document.body.classList.add("no_scroll")
    }

    const test = () => {
        console.log(calEvent)
    }

    const header = {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
    }

    return (
        <div>
            <button onClick={test}>Button</button>
            {
                modalIsOpen&&<ModalView handleModalInput={handleModalInput} closeModal={closeModal}/>
            }
            <FullCalendar 
                defaultView="timeGridWeek" 
                header={header}
                plugins={[ dayGridPlugin, interactionPlugin, timeGridPlugin, googleCalendarPlugin ]} 
                dateClick={handleDateClick} 
                googleCalendarApiKey='AIzaSyBJ_fDYcKDWH2tgFJ2SbTH1j4S2nJfRa88'
                events = {calEvent}
                nowIndicator={true}
                height="parent"
                longPressDelay
            />
        </div>
    )
}

export default FullCal
