import React, {useState, useContext, useEffect} from 'react'
import {DataContext} from '../../context/ContextProvider'

import IconLoader from '../Icons/IconLoader'

import SearchInput from '../FilterSection/SearchInput'
import List from './List';

import Fuse from 'fuse.js'
import SelectDropdown from '../FilterSection/SelectDropdown'

function CompanyListMobile() {

    const { dataFetched, companyData } = useContext(DataContext)
    const [ sortedList, setSortedList ] = useState([])
    const [ queryList, setQueryList ] = useState([])

    const [stateValues, setStateValues] = useState({
        country: [],
        mainWorkingSector: [],
        filterBadges: []
    })

    // const [ countries, setCountries ] = useState([])
    // const [ sector, setSector ] = useState([])
    // const [ badges, setBadges ] = useState([])

    const [ filterOpen, setFilterOpen ] = useState(false)

    useEffect( () => {

        // only run this filter when the data is fetched and is not null
        if(dataFetched && companyData != null){
            setSortedList(companyData.sort((a,b)=>{
                if(a.company.toLowerCase().trim() > b.company.toLowerCase().trim()) return 1;
                if(a.company.toLowerCase().trim() < b.company.toLowerCase().trim()) return -1;
                return 0;
              }))
            
            const countrylist = [...new Set(companyData.map( data => data.country).sort())].map(data => ({label: data, value: data}))
            // setCountries([{ label: 'All', value: "All"}, ...countrylist])
       
            const sectorList = [...new Set(companyData.map(data=>data.mainWorkingSector).sort())].map(data => ({label: data, value: data}))
            // setSector([{ label: 'All', value:"All"}, ...sectorList])

            const badgeList = [...new Set(companyData.map(badge => badge.filterBadges[0]).sort())].map(data=>({label: data, value: data}))
            // setBadges([{ label: 'All', value:"All"}, ...badgeList])
            setStateValues({
                country: [{ label: 'All', value: "All"}, ...countrylist],
                mainWorkingSector: [{ label: 'All', value:"All"}, ...sectorList],
                filterBadges: [{ label: 'All', value: "All"}, ...badgeList]
            })
            
            setQueryList(sortedList)
        }
    },[companyData, dataFetched, sortedList])


    const fuse = new Fuse(sortedList, {
        shouldSort: true,
        threshold: 0.1,
        keys: [
            {
                name: 'company',
                weight: 0.1
            },
            {
                name: 'mainWorkingSector',
                weight: 0.1
            },
            {
                name: 'country',
                weight: 0.1
            },
            {
                name: 'attendingGuests.name',
                weight: 0.1
            }
        ]
    })
    
    const handleInput = (e) => {
        const result = e ? fuse.search(e).map(result=>result.item) : sortedList
        setQueryList(result)
    }

    const onDropdownChange = (evt, meta) => {
        let newList = sortedList
        if(evt === null){
            setQueryList(sortedList)
        }else{
            evt.forEach( element => {
                newList = newList.filter(i => i[meta.name].includes(element.value) )
                setQueryList(newList)
            })
        }
        // if(evt.value === 'All' || evt.value === null){
        //     setQueryList(sortedList)
        // }else{
        //     const filterData = queryList.filter(i => i[meta.name] === evt.value)
        //     setQueryList(filterData)
        // }
        
    }

    const handleFilterOpen = () => {
        setFilterOpen(!filterOpen)
    }

    // function to render a list of elements
    function renderList() {

        if(dataFetched){
            return(
                <>
                    <div className='filter_menu'>
                        <span onClick={handleFilterOpen}>
                            {
                                filterOpen ? <IconLoader title='close' icon_className='close_icon' /> : <IconLoader title='list' icon_className='list_icon' />
                            }
                        </span>
                        <SearchInput onInput={handleInput} placeholder={'Type a name to search'}/>
                        <div className={`dropdown_filter ${filterOpen ? 'filter_open' : 'filter_close'}`}>
                            <SelectDropdown label={'Select a country'} name="country" onChange={onDropdownChange} options={stateValues.country} />
                            <SelectDropdown label={'Select a sector'} name="mainWorkingSector" onChange={onDropdownChange} options={stateValues.mainWorkingSector} />
                            <SelectDropdown label={'Select a badge'} name="filterBadges" onChange={onDropdownChange} options={stateValues.filterBadges} />
                        </div>
                    </div>
                    <List values={queryList} />
                </>
            )
        }else{
            return(
                <div className='loader'><IconLoader title='logo' icon_className='loader_company' /><h2>Loading</h2> </div>
            )
        }
    }

    
    return (
        renderList()
    )
}

export default CompanyListMobile
