import React, {useState} from 'react'
import {Link} from 'react-router-dom'

import IconLoader from '../Icons/IconLoader'

const List = ({values}) => {

    let [ showList, setShowList ] = useState(true)

    const renderCompany = () => {
        // if(values === undefined || values.length === 0){
        //     return(
        //         <div className={`companylist_mobile_list ${showList ? 'show' : ''}`}>
        //             <h1>No items found</h1>
        //         </div>
        //     )
        // }

        return (
            <>
                {/* <div className={`companylist_mobile_a-z ${showList ? 'show' : ''}`} onClick={showCompanies}>
                    <h1><strong>{letter}</strong></h1>
                    <IconLoader title='down arrow' icon_className='chevron' />
                </div> */}
                <div className={`companylist_mobile_list ${showList ? 'show' : ''}`}>
                {   values.map( (company, index) => (
                        <div className='companylist_mobile_list_item' key={index}>
                            <div className="companylist_mobile_list_item_top">
                                <div className="companylist_mobile_list_item_top_link">
                                    <Link to={{
                                        pathname:`/company/${company.id}`,
                                        state: {company}
                                    }} key={index}>
                                            <h2>{company.company}</h2>
                                    </Link>
                                </div>
                                <hr/>
                                <div className='companylist_mobile_list_item_bottom' >
                                    <div> 
                                        <p>{company.country || 'Not Available'}</p>
                                    </div>
                                    <div> 
                                        <p>{company.mainWorkingSector || 'Not Available'}</p>
                                    </div>
                                    <div> 
                                        <p>{company.email || 'Not Available'}</p>
                                    </div>
                                </div>
                                <hr/>
                                <div className='companylist_mobile_list_item_guests'>
                                <p className='guest_text'>Attending guests</p>
                                    <div className='guests'>
                                        {company.attendingGuests.slice(0, 2).map((guest, id) => (
                                            <div className='guest' key={id}>
                                                {/* <img className="guest_pic" src={image} alt="" /> */}
                                                <p>{guest.name}</p>
                                            </div>
                                        ) )}
                                    </div>
                                </div>
                            </div>
                            <div className='companylist_mobile_list_item_icon'>
                                <IconLoader title='star' icon_className='star' />
                            </div>
                        </div>
                    )
                
                )}
            </div>
            </>
        )
    }

    return (
        <>
            {renderCompany()}
        </>
    )
}

export default List
